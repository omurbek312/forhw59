<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class followerController extends Controller
{


    /**
     * @Route("/follow/{id}",name="follow")
     * @Method("GET")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(int $id, Request $request)
    {

        $user = $this->getDoctrine()->getManager();
        $followUser = $user->getRepository("AppBundle:User")->find($id);
        if (!$followUser->hasFollover($this->getUser())) {
            $followUser->addFollover($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($followUser);
            $em->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );

        } else {
            $followUser->removeFollover($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($followUser);
            $em->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );
        }
    }

    /**
     * @Route("/my_followers")
     */
    public function muFollowers()
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($this->getUser())->getFollovers();
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        $array =[];
        foreach ($user as $followUsers ){
            foreach ($users as $user1){
                if ($user1 == $followUsers){
                    $array[]= $user1;
                }
            }
        }
        return $this->render("@App/profile/followers.htm.twig", ["users" => $array]);
    }

}
