<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class indexController extends Controller
{
    /**
     * @Route("/",name="list")
     */
    public function indexAction()
    {
        if (!$this->getUser()) {
            return $this->redirect("/login");
        }
        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
        $users = $this->getDoctrine()->getRepository(User::class)->findAll();
        return $this->render('@App/index/index.html.twig', array(
            "user" => $user,
            'users' => $users
        ));

    }


}
