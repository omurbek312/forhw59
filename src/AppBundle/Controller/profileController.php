<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/profile")
 *
 */
class profileController extends Controller
{
    /**
     * @Route("/",name="profile")
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myProfile(Request $request)
    {

        $user = $this->getDoctrine()->getRepository(User::class)->find($this->getUser()->getId());
        $post = new Post();
        $form_builder= $this->createForm(PostType::class,$post);
        $form_builder->handleRequest($request);
        if ($form_builder->isSubmitted() && $form_builder->isValid()) {
        $post->setAuthor($this->getUser());
        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();
            return $this->redirectToRoute('profile');
        }
        return $this->render('@App/profile/profile.html.twig', array(
            "user" => $user,
            "form_post"=>$form_builder->createView()
        ));
    }

    /**
     * @Route("/{id}",name="dataUser")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     * @Method("GET")
     */
    public function clickProfile(int $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        return $this->render('@App/profile/lookedProfile.html.twig', array(
            "user" => $user
            ));
    }

}
