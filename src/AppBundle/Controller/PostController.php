<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class PostController extends Controller
{
    /**
     * @Route("/liked/{id}",name="liked")
     * @Method("GET")
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addLike(int $id,Request $request)
    {
        $post = $this->getDoctrine()->getRepository('AppBundle:Post');
        $postLike = $post->find($id);

        if (!$postLike->hasLiker($this->getUser())) {
            $postLike->addLiker($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($postLike);
            $em->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );
        } else {
            $postLike->removeLiker($this->getUser());
            $em = $this->getDoctrine()->getManager();
            $em->persist($postLike);
            $em->flush();
            return $this->redirect(
                $request
                    ->headers
                    ->get('referer')
            );
        }
    }

    /**
     * @Route("/users")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function allUsers()
    {

        $i_am = $this->getUser();
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($i_am)->getFollovedUsers();
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        $array =[];
        foreach ($user as $followUsers ){
            foreach ($users as $user1){
                if ($user1 == $followUsers){
                   $array[]= $user1;
                }
            }
    }
        return $this->render("@App/Post/post.html.twig", ["users" => $array]);
    }

}
