<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;


/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="fos_user")
 * @Vich\Uploadable
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     *
     * @Vich\UploadableField(mapping="avatar_file", fileNameProperty="avatar")
     *
     * @var File
     */
    protected $imageFile;
    /**
     * @var string
     * @ORM\Column(name="avatar", type="string", nullable=false)
     */
    protected $avatar;
    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Post",mappedBy="author")
     */
    private $posts;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Post",mappedBy="likers")
     */
    private $likedPosts;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User",mappedBy="follovers")
     */
    private $follovedUsers;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="User",inversedBy="follovedUsers")
     */
    private $follovers;

    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
        $this->follovedUsers = new ArrayCollection();
        $this->follovers = new ArrayCollection();
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }


    /**
     * @param string $avatar
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
        return $this;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }


    /**
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {
        $this->posts->add($post);
    }

    /**
     * @param Post $post
     */
    public function removePost(Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }

    /**
     * @param Post $post
     */
    public function addLikedPost(Post $post)
    {
        $this->likedPosts->add($post);
    }
    /**
     * @param Post $post
     */

    public function removeLikedPost(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }

    /**
     * @return ArrayCollection
     */
    public function getFollovedUsers()
    {
        return $this->follovedUsers;
    }

    /**
     * @return ArrayCollection
     */
    public function getFollovers()
    {
        return $this->follovers;
    }

    /**
     * @param User $user
     */
    public function addFollover(User $user){
        $this->follovers->add($user);
    }
    /**
     * @param User $user
     */
    public function removeFollover(User $user){
        $this->follovers->removeElement($user);
    }
    /**
     * @param User $user
     * @return bool
     */
    public function hasFollover(User $user){
        return  $this->follovers->contains($user);
    }
    /**
     * @param User $user
     */
    public function addFollovedUsers(User $user){
        $this->follovedUsers->add($user);
    }
    /**
     * @param User $user
     */
    public function removeFollovedUsers(User $user){
        $this->follovedUsers->removeElement($user);
    }

}
