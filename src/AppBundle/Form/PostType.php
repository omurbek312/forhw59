<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
           $builder->add('imageFile',FileType::class,['label'=>false,'attr'=>['class'=>"sendPost"]])
            ->add("description",TextareaType::class,['label'=>false,'required' => false,'attr'=>['class'=>'description','placeholder' => 'Описание']])
            ->add("submit",SubmitType::class,['label'=>'Опубликовать','attr'=>['class'=>'sendPost']]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'app_bundle_post_type';
    }
}
